var searchData=
[
  ['dif_0',['dif',['../classencoder_1_1_encoder.html#a3ac0e6df2fdc7148732f392426193ee0',1,'encoder::Encoder']]],
  ['disable_1',['disable',['../classdrv8847_1_1_d_r_v8847.html#a13335d0117e56c05b137294b6a73f725',1,'drv8847::DRV8847']]],
  ['drv8847_2',['DRV8847',['../classdrv8847_1_1_d_r_v8847.html',1,'drv8847']]],
  ['drv8847_2epy_3',['drv8847.py',['../drv8847_8py.html',1,'']]],
  ['duties_4',['duties',['../main__term_8py.html#ac305985e431ea36b996f244a7a519b03',1,'main_term']]],
  ['duty_5',['duty',['../classmotor_1_1_motor.html#a2a58ebbc198a13f3b8813f1eee640073',1,'motor::Motor']]],
  ['duty_5fx_6',['duty_x',['../classclosed_loop_1_1_closed_loop.html#a2490d8a3fd8a724edd911b08864f0a8b',1,'closedLoop::ClosedLoop']]],
  ['duty_5fy_7',['duty_y',['../classclosed_loop_1_1_closed_loop.html#ad764caa55509d32cfac7c414d19b68a8',1,'closedLoop::ClosedLoop']]],
  ['dutyxarray_8',['dutyXArray',['../task_user_8py.html#a3537499d72429ad0d78afb3062b40d63',1,'taskUser']]],
  ['dutyyarray_9',['dutyYArray',['../task_user_8py.html#a393f06b25bf04a0c5ea91095dea4bfd9',1,'taskUser']]],
  ['dvar_10',['dVar',['../main__lab2_8py.html#a7810bed7cd65bac1d1df3de727efae03',1,'main_lab2.dVar()'],['../main__lab3_8py.html#ab9139b8085976459ca3645fb59a39c6f',1,'main_lab3.dVar()'],['../main__lab4_8py.html#a455f305c258786c35d00b799dd415507',1,'main_lab4.dVar()']]]
];
